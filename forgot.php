<html lang="en">

	  <head>

		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="description" content="">
		<meta name="author" content="">

		<title>Aplikasi Inventaris Sarana dan Prasarana</title>

		<!-- Bootstrap core CSS-->
		<link href="admin/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

		<!-- Custom fonts for this template-->
		<link href="admin/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
		<!-- Page level plugin CSS-->
		<!-- Custom styles for this template-->
		<link href="admin/css/sb-admin.css" rel="stylesheet">
		<link rel="shortcut icon" href="admin/gambar/1.jpg"><!--icon untuk di atas--->		
		 <link href="admin/vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
	  </head>
	<?php
	 function anti ($data)
	 {
		   $filter = mysql_real_escape_string(stripcslashes(strip_tags(htmlspecialchars($data,ENT_QUOTES))));
		   return $filter;
	 }
	 ?>
	  <body id="page-top">
		<nav class="navbar navbar-expand navbar-dark bg-dark static-top">
		  <a class="navbar-brand mr-1" href="index.php">Inventaris Sarana dan Prasarana SMK</a>
		  <!-- Navbar Search -->
		  <form class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">
		  </form>
		  <!-- Navbar -->
		  <ul class="navbar-nav ml-auto ml-md-0">
			<a class="navbar-brand mr-1" href="#">
				<script type="text/javascript">
			 //fungsi displayTime yang dipanggil di bodyOnLoad dieksekusi tiap 1000ms = 1detik
		function tampilkanwaktu(){
			//buat object date berdasarkan waktu saat ini
			var waktu = new Date();
			//ambil nilai jam, 
			//tambahan script + "" supaya variable sh bertipe string sehingga bisa dihitung panjangnya : sh.length
			var sh = waktu.getHours() + ""; 
			//ambil nilai menit
			var sm = waktu.getMinutes() + "";
			//ambil nilai detik
			var ss = waktu.getSeconds() + "";
			//tampilkan jam:menit:detik dengan menambahkan angka 0 jika angkanya cuma satu digit (0-9)
			document.getElementById("clock").innerHTML = (sh.length==1?"0"+sh:sh) + ":" + (sm.length==1?"0"+sm:sm) + ":" + (ss.length==1?"0"+ss:ss);
		}
	</script>
	<body onload="tampilkanwaktu();setInterval('tampilkanwaktu()', 1000);">	
	<span id="clock"></span> 
	<?php
	$hari = date('l');
	/*$new = date('l, F d, Y', strtotime($Today));*/
	if ($hari=="Sunday") {
		echo "Minggu";
	}elseif ($hari=="Monday") {
		echo "Senin";
	}elseif ($hari=="Tuesday") {
		echo "Selasa";
	}elseif ($hari=="Wednesday") {
		echo "Rabu";
	}elseif ($hari=="Thursday") {
		echo("Kamis");
	}elseif ($hari=="Friday") {
		echo "Jum'at";
	}elseif ($hari=="Saturday") {
		echo "Sabtu";
	}
	?>,
	<?php
	$tgl =date('d');
	echo $tgl;
	$bulan =date('F');
	if ($bulan=="January") {
		echo " Januari ";
	}elseif ($bulan=="February") {
		echo " Februari ";
	}elseif ($bulan=="March") {
		echo " Maret ";
	}elseif ($bulan=="April") {
		echo " April ";
	}elseif ($bulan=="May") {
		echo " Mei ";
	}elseif ($bulan=="June") {
		echo " Juni ";
	}elseif ($bulan=="July") {
		echo " Juli ";
	}elseif ($bulan=="August") {
		echo " Agustus ";
	}elseif ($bulan=="September") {
		echo " September ";
	}elseif ($bulan=="October") {
		echo " Oktober ";
	}elseif ($bulan=="November") {
		echo " November ";
	}elseif ($bulan=="December") {
		echo " Desember ";
	}
	$tahun=date('Y');
	echo $tahun;
	?>
	</a>
	&nbsp;
	&nbsp;
	&nbsp;
	&nbsp;
			<li class="nav-item dropdown no-arrow mx-1">
			 <a class="navbar-brand mr-1" href="login.php">Login</a>
			</li>			
		  </ul>
		</nav>
      <div id="content-wrapper">
        <div class="container-fluid">
			 <div class="container">
      <div class="card card-login mx-auto mt-5">
        <div class="card-header">Lupa Password</div>
        <div class="card-body">
         <form>
			<div class="form-group">
					<input class="form-control" placeholder="Masukan Email Anda" autocomplete="off"  name="gmail" type="email" value="" maxlength="22" required="">
			</div>
			<a class="btn btn-dark btn-block" href="login.html">Reset Password</a>
		</form>
           <div class="text-center">
							<a class="d-block small mt-3" href="login.php">Login</a>
					</div>
        </div>
      </div>
    </div> 
        </div>
      </div>
    </div>
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fas fa-angle-up"></i>
    </a>
 
  </body>

</html>
