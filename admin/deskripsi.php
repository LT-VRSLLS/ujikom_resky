<?php
include 'header.php';
?>
<?php
include 'cek_level.php';
?>
    <div id="wrapper">

      <!-- Sidebar -->
     <?php
	 include 'menu.php';
	 ?>

      <div id="content-wrapper">

        <div class="container-fluid">

          <!-- Breadcrumbs-->
           <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="index.php">Beranda</a>
            </li>
            <li class="breadcrumb-item active">Halaman Beranda</li>
          </ol>

          <!-- Page Content -->
          <h3>Deskripsi</h3>
          <hr>
		  <a href="tambah_deskripsi.php" class="btn btn-primary fa fa-plus pull-right" style="margin-left: 900px">Tambah Deskripsi</a>
		  <br>
		  <br>
          <div class="card mb-3">
            <div class="card-header">
              <i class="fas fa-table"></i>
              Deskripsi</div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr><th>No</th>
							<th>Deskripsi</th>
							<th>Gambar</th>
							<th>Aksi</th>
                    </tr>
                  </thead>
                  
				  <tbody>
                  <?php
							include "koneksi.php";
							$no=1;
							$select=mysqli_query($koneksi,"select * from deskripsi");
							while($data=mysqli_fetch_array($select))
									{
							?>
										
										<tr class="succes">
											<td><?php echo $no++ ?></td>
											<td><?php echo $data['kata']; ?></td>
											<td><img src="images/<?php echo $data['foto'] ?>" width="110" height="150"></td>
											
												<td>
													<a href="edit_deskripsi.php?id=<?php echo $data['id']; ?>"><button type="button" class="btn btn-outline btn-primary fa fa-edit"></button></a>
													<a href="hapus_deskripsi.php?id=<?php echo $data['id']; ?>"><button type="button" class="btn btn-outline btn-danger fa fa-trash"></button></a> 
                                
												</td>
										</tr>
											<?php	
									}
											?>
					</tbody>
                </table>
              </div>
            </div>
          </div>
					
        </div>
        <!-- /.container-fluid -->

        <!-- Sticky Footer -->
        <?php include 'footer.php' ?>

      </div>
      <!-- /.content-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel"><?php echo $_SESSION['nama_petugas'];?> ,Yakin Ingin Keluar?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Silahkan Klik Button Logout</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Kembali</button>
            <a class="btn btn-primary" href="logout.php">Logout</a>
          </div>
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
	
	 <script src="vendor/datatables/jquery.dataTables.js"></script>
    <script src="vendor/datatables/dataTables.bootstrap4.js"></script>
	
    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin.min.js"></script>
	
	<script src="js/demo/datatables-demo.js"></script>

  </body>

</html>
