<?php
include 'header.php';
?>
<?php
include 'cek_level2.php';
?>
    <div id="wrapper">

      <!-- Sidebar -->
    <?php
	include 'menu.php';
	?>
      <div id="content-wrapper">

        <div class="container-fluid">

          <!-- Breadcrumbs-->
           <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="index.php">Beranda</a>
            </li>
            <li class="breadcrumb-item active">Halaman Beranda</li>
          </ol>

          <!-- Page Content -->
          <h3>Pengembalian</h3>
          <hr>
		   <center><div class="panel-body">
						<div class="col-lg-5">
						<label>Pilih Kode Peminjaman</label>
							<form method="POST">
							<select name="id_peminjaman" class="form-control m-bot15">
								<?php
								include "koneksi.php";
								//display values in combobox/dropdown
								$result = mysqli_query($koneksi,"SELECT id_peminjaman,kd_pinjam from peminjaman where status_peminjaman='Pinjam' ");
								while($row = mysqli_fetch_assoc($result))
								{
								echo "<option value='$row[id_peminjaman]'>$row[kd_pinjam]</option>";
								} 
								?>
									</select>
									<br/>
								<button type="submit" name="pilih" class="btn btn-outline btn-primary">Tampilkan</button>
							</form></center>
							<br>
			              <?php
							if(isset($_POST['pilih'])){?>				
			<div class="card mb-3">
            <div class="card-header">
              <i class="fas fa-table"></i>
              Data Pinjam</div>
            <div class="card-body">
                                <form action="proses_pengembalian.php" method="post" role="form">
								<div class="table-responsive">
								<table class='table table-bordered'>
								<tr>
                                    <td>Kode Peminjam</td>
                                    <td>Nama Barang</td>
                                    <td>Jumlah Pinjam</td>
                                    <td>Tanggal Pinjam</td>
                                    <td>Nama Pegawai</td>
                                    <td>Aksi</td>
								&nbsp;
							                                               <?php
										include "koneksi.php";
										$id_peminjaman=$_POST['id_peminjaman'];
										$select=mysqli_query($koneksi,"select * from peminjaman i left join detail_pinjam p on p.id_detail_pinjam=i.id_peminjaman
																						left join inventaris v on p.id_inventaris=v.id_inventaris
																						left join pegawai g on i.id_pegawai=g.id_pegawai
																						where id_peminjaman='$id_peminjaman' AND status ='Y'");
										while($data=mysqli_fetch_array($select)){
										?>
                                      	<tr>
							
                                            <td><input name="kd_pinjam" type="text" class="form-control"  value="<?php echo $data['kd_pinjam'];?>" readonly="readonly"></td>
											<td><input name="id_inventaris[]" type="hidden" class="form-control"  value="<?php echo $data['id_inventaris'];?> " autocomplete="off" readonly="readonly">
												<input name="" type="text" class="form-control" placeholder="Masukan Jumlah barang" value="<?php echo $data['nama'];?>" autocomplete="off" maxlength="11" required="" readonly="readonly">
												<input name="id_peminjaman" type="hidden" class="form-control" placeholder="Masukan Jumlah barang" value="<?php echo $data['id_peminjaman'];?>" autocomplete="off" maxlength="11" required="" readonly="readonly">
												<input name="id" type="hidden" class="form-control" placeholder="Masukan Jumlah barang" value="<?php echo $data['id'];?>" autocomplete="off" maxlength="11" required="" readonly="readonly">
												<input name="id_detail_pinjam[]" type="hidden" class="form-control" placeholder="Masukan Jumlah barang" value="<?php echo $data['id_detail_pinjam'];?>" autocomplete="off" maxlength="11" required="" readonly="readonly"></td>
                                            <td><input name="jumlah[]" id="jumlah" type="text" class="form-control" placeholder="" value="<?php echo $data['jumlah_pinjam'];?>" autocomplete="off" maxlength="11" readonly="readonly"></td>
											<td><input name="tanggal_pinjam" type="text" class="form-control"  value="<?php echo $data['tanggal_pinjam'];?>" readonly="readonly"></td>
											<td><input name="status_peminjaman" type="hidden" class="form-control" placeholder="Masukan Jumlah barang" value="<?php echo $data['status_peminjaman'];?>" autocomplete="off" maxlength="11" required="">
												<input name="status[]" type="hidden" class="form-control" placeholder="Masukan Jumlah barang" value="<?php echo $data['status'];?>" autocomplete="off" maxlength="11" required="">
												<input name="tanggal_kembali" type="hidden" class="form-control" placeholder="Masukan Jumlah barang" autocomplete="off" maxlength="11" required="">
												<input name="" type="text" class="form-control"value="<?php echo $data['nama_pegawai'];?>" autocomplete="off" maxlength="11" required="" readonly="readonly">
											<input name="id_pegawai" id="jumlah" type="hidden" class="form-control" placeholder="" value="<?php echo $data['id_pegawai'];?>" autocomplete="off" maxlength="11" readonly="readonly"></td>
											
										<td><a href="hapus_pmj.php?id=<?php echo $data['id']; ?>"><button type="button" class="btn btn-outline btn-warning fa fa-sending">Kembali</button></a></td>
                                        </tr>
									<?php } ?>
									
								</table>
								<button type="submit"  class="btn btn-success">Kembalikan Semua</button>
                                 </form>
            </div>
          </div>
		  <?php } ?>

			<br>
			<hr>			
          <div class="card mb-3">
            <div class="card-header">
              <i class="fas fa-table"></i>
             Table Pengembalian</div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
						<tr><th>No</th>
							<th>Kode Peminjaman</th>
							<th>Tanggal Pinjam</th>
							<th>Tanggal Kembali</th>
							<th>Status Peminjaman</th>
							<th>Nama Pegawai</th>
							<th>Aksi</th>
                    </tr>
                  </thead>
                  
				  <tbody>
                  <?php
							include "koneksi.php";
							$no=1;
							$select=mysqli_query($koneksi,"select * from peminjaman left join pegawai on pegawai.id_pegawai=peminjaman.id_pegawai where status_peminjaman='Kembali'");
							while($data=mysqli_fetch_array($select))
									{
							?>
										
										<tr class="succes">
											<td><?php echo $no++ ?></td>
											<td><?php echo $data['kd_pinjam']; ?></td>
											<td><?php echo $data['tanggal_pinjam']; ?></td>
											<td><?php echo $data['tanggal_kembali']; ?></td>
											<td><?php echo $data['status_peminjaman']; ?></td>
											<td><?php echo $data['nama_pegawai']; ?></td>
											
												<td>													
													<a href="hapus_pengembalian.php?id_peminjaman=<?php echo $data['id_peminjaman']; ?>"><button type="button" class="btn btn-outline btn-danger fa fa-trash"></button></a>
												</td>
										</tr>
											<?php	
									}
											?>
					</tbody>
                </table>
              </div>
            </div>
            
          </div>

        </div>
        <!-- /.container-fluid -->

        <!-- Sticky Footer -->
        <?php include 'footer.php' ?>

      </div>
      <!-- /.content-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel"><?php echo $_SESSION['nama_petugas'];?> ,Yakin Ingin Keluar?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Silahkan Klik Button Logout</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Kembali</button>
            <a class="btn btn-primary" href="logout.php">Logout</a>
          </div>
        </div>
      </div>
    </div>

<?php
include 'script.php';
?>

  </body>

</html>
