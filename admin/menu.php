<?php
            
	if ($_SESSION['id_level']==1){
	echo'<ul class="sidebar navbar-nav">
	  <li class="nav-item">
          <a class="nav-link" href="#">
            <i class=""></i>
            <h3>Administrator</h3>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="index.php">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span>
          </a>
        </li>
        
        <li class="nav-item">
          <a class="nav-link" href="inventaris.php">
            <i class="fas fa-fw fa-chart-area"></i>
            <span>Inventaris</span></a>
        </li>
		
        <li class="nav-item">
          <a class="nav-link" href="peminjaman.php">
            <i class="fas fa-fw fa-share"></i>
            <span>Peminjaman</span></a>
        </li>
		
		<li class="nav-item">
          <a class="nav-link" href="pengembalian.php">
            <i class="fas fa-fw fa-reply"></i>
            <span>Pengembalian</span></a>
        </li>
		
		<li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-fw fa-file"></i>
            <span>Laporan</span>
          </a>
          <div class="dropdown-menu" aria-labelledby="pagesDropdown">
            <a class="dropdown-item" href="inventaris.php">L.Inventaris</a>
            <a class="dropdown-item" href="peminjaman.php">L.Peminjaman</a>
            <a class="dropdown-item" href="pengembalian.php">L.Pengembalian</a>
            <div class="dropdown-divider"></div>
          </div>
        </li>
		
		 <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-fw fa-folder"></i>
            <span>Lainnya</span>
          </a>
          <div class="dropdown-menu" aria-labelledby="pagesDropdown">
            <h6 class="dropdown-header"> Deskripsi:</h6>
            <a class="dropdown-item" href="deskripsi.php">Deskripsi</a>
            <div class="dropdown-divider"></div>
            <h6 class="dropdown-header">Master Data:</h6>
            <a class="dropdown-item" href="level.php">Level</a>
            <a class="dropdown-item" href="pegawai.php">Pegawai</a>
            <a class="dropdown-item" href="petugas.php">Petugas</a>
		   <div class="dropdown-divider"></div>
            <h6 class="dropdown-header">Inventarisasi:</h6>
            <a class="dropdown-item" href="ruang.php">Ruang</a>
            <a class="dropdown-item" href="jenis.php">Jenis</a>
          </div>
        </li>
	</ul>';}
	
	elseif ($_SESSION['id_level']==2){
	echo'<ul class="sidebar navbar-nav">
	  <li class="nav-item">
          <a class="nav-link" href="#">
            <i class=""></i>
            <h3>Operator</h3>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="index.php">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span>
          </a>
        </li>
        
        <li class="nav-item">
          <a class="nav-link" href="peminjaman.php">
            <i class="fas fa-fw fa-share"></i>
            <span>Peminjaman</span></a>
        </li>
		
		<li class="nav-item">
          <a class="nav-link" href="pengembalian.php">
            <i class="fas fa-fw fa-reply"></i>
            <span>Pengembalian</span></a>
        </li>
		
	</ul>';}
	
	elseif ($_SESSION['id_level']==3){
	echo'<ul class="sidebar navbar-nav">
	  <li class="nav-item">
          <a class="nav-link" href="#">
            <i class=""></i>
            <h3>Peminjam</h3>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="index.php">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span>
          </a>
        </li>
        
        	
        <li class="nav-item">
          <a class="nav-link" href="peminjaman.php">
            <i class="fas fa-fw fa-share"></i>
            <span>Peminjaman</span></a>
        </li>
		
	</ul>';}
	?>