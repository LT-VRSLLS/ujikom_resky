<?php
include 'header.php';
?>
<?php
include 'cek_level.php';
?>
    <div id="wrapper">

      <!-- Sidebar -->
     <?php
	 include 'menu.php';
	 ?>

      <div id="content-wrapper">

        <div class="container-fluid">

          <!-- Breadcrumbs-->
           <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="index.php">Beranda</a>
            </li>
            <li class="breadcrumb-item active">Halaman Beranda</li>
          </ol>

          <!-- Page Content -->
          <h3>Petugas</h3>
          <hr>
			<center><h5>Tambah Petugas</h5></center>
			<hr>
          <form action="simpan_petugas.php" method="post">
			<div class="col-md-6">
				<div class="form-group">
				<label>Username</label>
				<input name="username" type="text" class="form-control" placeholder="Masukan Username" autocomplete="off" maxlength="22" required="">
				</div>
			 </div>
			 <div class="col-md-6">
				<div class="form-group">
				<label>Password</label>
				<input name="password" type="text" class="form-control" placeholder="Masukan Password" autocomplete="off" maxlength="22" required="">
				</div>
			 </div>
			 <div class="col-md-6">
				<div class="form-group">
				<label>Nama Petugas</label>
				<input name="nama_petugas" type="text" class="form-control" placeholder="Masukan Nama Petugas" autocomplete="off" maxlength="22" required="">
				</div>
			 </div>
			 <div class="col-md-6">
				<div class="form-group">
				<label>Id Level</label>
				<select name="id_level" class="form-control m-bot15">
								<option>---Pilih---</option>
								<?php
								include "koneksi.php";
								//display values in combobox/dropdown
								$result = mysqli_query($koneksi,"SELECT id_level,nama_level from level ");
								while($row = mysqli_fetch_assoc($result))
								{
								echo "<option>$row[id_level].$row[nama_level]</option>";
								} 
								?>
								
									</select>
				</div>
			 </div>
			 <div class="col-md-6">
				<div class="form-group">
				<label>Id Pegawai</label>
				<select name="id_pegawai" class="form-control m-bot15">
								<option>---Pilih---</option>
								<?php
								include "koneksi.php";
								//display values in combobox/dropdown
								$result = mysqli_query($koneksi,"SELECT id_pegawai,nama_pegawai from pegawai ");
								while($row = mysqli_fetch_assoc($result))
								{
								echo "<option>$row[id_pegawai].$row[nama_pegawai]</option>";
								} 
								?>
								
									</select>
				</div>
			 </div>
			 
             <div class="col-md-6">
								<button type="submit" class="btn btn-primary">Simpan</button>
								<a href="petugas.php"><button type="button" class="btn btn-">Kembali</button>
			</div>
          </form>
     
        
        <!-- /.container-fluid -->

        <!-- Sticky Footer -->
        <?php include 'footer.php' ?>

      </div>
      <!-- /.content-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel"><?php echo $_SESSION['nama_petugas'];?> ,Yakin Ingin Keluar?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Silahkan Klik Button Logout</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Kembali</button>
            <a class="btn btn-primary" href="logout.php">Logout</a>
          </div>
        </div>
      </div>
    </div>

    <?php
	include 'script.php';
	?>

  </body>

</html>
