<?php 
include 'database_petugas.php';
$db = new database();
?>
<?php
include 'header.php';
?>
<?php
include 'cek_level.php';
?>
    <div id="wrapper">

      <!-- Sidebar -->
     <?php
	 include 'menu.php';
	 ?>

      <div id="content-wrapper">

        <div class="container-fluid">

          <!-- Breadcrumbs-->
           <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="index.php">Beranda</a>
            </li>
            <li class="breadcrumb-item active">Halaman Beranda</li>
          </ol>

          <!-- Page Content -->
          <h3>Petugas</h3>
          <hr>
			<center><h5>Edit Petugas</h5></center>
			<hr>
          <form action="proses_petugas.php?aksi=update" method="post">
									<?php
									foreach($db->edit($_GET['id_petugas']) as $d){
									?>
			<div class="col-md-6">
				<div class="form-group">
				<label>Username</label>
				<input name="id_petugas" type="hidden" class="form-control" placeholder="Masukan Username" value="<?php echo $d['id_petugas'];?>" autocomplete="off" maxlength="22" required="">
				<input name="username" type="text" class="form-control" placeholder="Masukan Username" value="<?php echo $d['username'];?>" autocomplete="off" maxlength="22" required="">
				</div>
			 </div>
			 <div class="col-md-6">
				<div class="form-group">
				<label>Password</label>
				<input name="password" type="text" class="form-control" placeholder="Masukan Password" value="<?php echo $d['password'];?>" autocomplete="off" maxlength="22" required="">
				</div>
			 </div>
			 <div class="col-md-6">
				<div class="form-group">
				<label>Nama Petugas</label>
				<input name="nama_petugas" type="text" class="form-control" placeholder="Masukan Nama Petugas" value="<?php echo $d['nama_petugas'];?>" autocomplete="off" maxlength="22" required="">
				</div>
			 </div>
			 <div class="col-md-6">
				<div class="form-group">
				<label>Id Level</label>
				<select name="id_level" class="form-control m-bot15">
								<option><?php echo $d['id_level'];?></option>
								<?php
								include "koneksi.php";
								//display values in combobox/dropdown
								$result = mysql_query("SELECT id_level,nama_level from level ");
								while($row = mysql_fetch_assoc($result))
								{
								echo "<option>$row[id_level].$row[nama_level]</option>";
								} 
								?>
								
									</select>
				</div>
			 </div>
			 <div class="col-md-6">
				<div class="form-group">
				<label>Id Pegawai</label>
				<select name="id_pegawai" class="form-control m-bot15">
								<option><?php echo $d['id_pegawai'];?></option>
								<?php
								include "koneksi.php";
								//display values in combobox/dropdown
								$result = mysql_query("SELECT id_pegawai,nama_pegawai from pegawai ");
								while($row = mysql_fetch_assoc($result))
								{
								echo "<option>$row[id_pegawai].$row[nama_pegawai]</option>";
								} 
								?>
								
									</select>
				</div>
			 </div>
			 
             <div class="col-md-6">
								<button type="submit" class="btn btn-primary">Update</button>
								<a href="petugas.php"><button type="button" class="btn btn-">Kembali</button>
			</div>
									<?php } ?>
          </form>
     
        
        <!-- /.container-fluid -->

        <!-- Sticky Footer -->
        <?php include 'footer.php' ?>

      </div>
      <!-- /.content-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel"><?php echo $_SESSION['nama_petugas'];?> ,Yakin Ingin Keluar?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Silahkan Klik Button Logout</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Kembali</button>
            <a class="btn btn-primary" href="logout.php">Logout</a>
          </div>
        </div>
      </div>
    </div>

    <?php
	include 'script.php';
	?>

  </body>

</html>
