<?php 
include 'database_pegawai.php';
$db = new database();
?>
<?php
include 'header.php';
?>
    <div id="wrapper">

      <?php
   include 'menu.php';
   ?>

      <div id="content-wrapper">

        <div class="container-fluid">

          <!-- Breadcrumbs-->
           <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="index.php">Beranda</a>
            </li>
            <li class="breadcrumb-item active">Halaman Beranda</li>
          </ol>

          <!-- Page Content -->
          <h3>Pegawai</h3>
          <hr>
		  <a href="tambah_pegawai.php" class="btn btn-primary fa fa-plus pull-right" style="margin-left: 900px">Tambah Pegawai</a>
		  <br>
		  <br>
          <div class="card mb-3">
            <div class="card-header">
              <i class="fas fa-table"></i>
              Pegawai</div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
							<th>No</th>
							<th>Nama Pegawai</th>
							<th>Nip</th>
							<th>Alamat</th>
							<th>Aksi</th>
                    </tr>
                  </thead>
                  
				  <tbody>
                  
									<?php
									$no = 1;
									foreach($db->tampil_data() as $x){
									?>
										
										<tr class="succes">
											<td><?php echo $no++ ?></td>
											<td><?php echo $x['nama_pegawai']; ?></td>
											<td><?php echo $x['nip']; ?></td>
											<td><?php echo $x['alamat']; ?></td>
											
												<td>
													<a class="btn btn-outline btn-primary fa fa-edit" href="edit_pegawai.php?id_pegawai=<?php echo $x['id_pegawai']; ?>&aksi=edit"></a>
													<a class="btn btn-outline btn-danger fa fa-trash" href="hapus_pegawai.php?id_pegawai=<?php echo $x['id_pegawai']; ?>&aksi=hapus"></a> 
                                
												</td>
										</tr>
											<?php	
									}
											?>
					</tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        <!-- /.container-fluid -->

        <!-- Sticky Footer -->
        <?php include 'footer.php' ?>

      </div>
      <!-- /.content-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel"><?php echo $_SESSION['nama_petugas'];?> ,Yakin Ingin Keluar?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Silahkan Klik Button Logout</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Kembali</button>
            <a class="btn btn-primary" href="logout.php">Logout</a>
          </div>
        </div>
      </div>
    </div>

   <?php
   include 'script.php';
   ?>

  </body>

</html>
