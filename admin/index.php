<?php
include 'header.php';
?>

    <div id="wrapper">

      <!-- Sidebar -->
      <?php
	  include 'menu.php';
	  ?>
	
 <div id="content-wrapper">

        <div class="container-fluid">

          <!-- Breadcrumbs-->
           <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="index.php">Beranda</a>
            </li>
            <li class="breadcrumb-item active">Halaman Beranda</li>
          </ol>
		  <h3>Dashboard</h3>
          <hr>
			<?php 
			if($_SESSION['id_level'] == 1): ?>
			<?php echo ' <div class="alert alert-success">
							<button type="button" class="close" data-dismiss="alert">&times;</button>
                            <h5>Selamat Datang</h5>
                        	<h6>Di Administrator Aplikasi Sarana dan Prasarana</div></h6>
							';?>
			<?php endif; ?>
			
			<?php 
			if($_SESSION['id_level'] == 2): ?>
			<?php echo '<div class="alert alert-success">
							<button type="button" class="close" data-dismiss="alert">&times;</button>
                            <h5>Selamat Datang</h5>
                        	<h6>Di Operator Aplikasi Sarana dan Prasarana</div></h6>';?>
			<?php endif; ?>
			
			<?php 
			if($_SESSION['id_level'] == 3): ?>
			<?php echo '<div class="alert alert-success">
							<button type="button" class="close" data-dismiss="alert">&times;</button>
                            <h5>Selamat Datang</h5>
                        	<h6>Di Peminjaman Aplikasi Sarana dan Prasarana</div></h6>';?>
			<?php endif; ?>
			
        <!-- /.container-fluid -->

        <!-- Sticky Footer -->
        <?php include 'footer.php' ?>

      </div>
      <!-- /.content-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel"><?php echo $_SESSION['nama_petugas'];?> ,Yakin Ingin Keluar?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Silahkan Klik Button Logout</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Kembali</button>
            <a class="btn btn-primary" href="logout.php">Logout</a>
          </div>
        </div>
      </div>
    </div>

		<?php
		include 'script.php';
		?>

  </body>

</html>
